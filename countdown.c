///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
// Reference time: Sun Apr  4 00:00:00 2021
// Years: 0         Days: 54        Hours: 0        Minutes: 48     Seconds 40
// Years: 0         Days: 54        Hours: 0        Minutes: 48     Seconds 39
// Years: 0         Days: 54        Hours: 0        Minutes: 48     Seconds 38
// Years: 0         Days: 54        Hours: 0        Minutes: 48     Seconds 37
// Years: 0         Days: 54        Hours: 0        Minutes: 48     Seconds 36
// Years: 0         Days: 54        Hours: 0        Minutes: 48     Seconds 35
// Years: 0         Days: 54        Hours: 0        Minutes: 48     Seconds 34
//
// @author Shannon Kam <skam7@hawaii.edu>
// @date   02_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

struct tm reference;

int main(){
   long diff_year, diff_day, diff_hour, diff_min, diff_sec;
   time_t ref;
   time_t current;
   //create a reference time
   reference.tm_year = (2021 - 1900); //years since 1900
   reference.tm_mon = (4 - 1);        //months since Jan
   reference.tm_mday = 4;            //day of the month
   reference.tm_hour = 0;             //hour 0-23 20
   reference.tm_min = 0;              //min 0-59 25
   reference.tm_sec = 0;              //sec 0-59
   
   ref = mktime(&reference);

   //print reference time
   printf("Reference time: %s", asctime(&reference));

   mktime(&reference);
   
   while(1){
      current = time(NULL);
      
      diff_sec = difftime(ref, current);
      
      if(diff_sec < 0)
         diff_sec = diff_sec*(-1);
      //calculate difference
      diff_year = diff_sec/31536000;            //sec/year = 365*24*60*60 = 31536000
      diff_sec = diff_sec - (diff_year*31536000);
   
      diff_day = diff_sec/86400;           //sec/day = 24*60*60 = 86400
      diff_sec = diff_sec - (diff_day*86400);

      diff_hour = diff_sec/3600;             //sec/hour = 60*60 = 3600
      diff_sec = diff_sec - (diff_hour*3600);

      diff_min = diff_sec/60;                   //sec/min = 60
      diff_sec = diff_sec - (diff_min*60);
   
      printf("Years: %ld\t Days: %ld\t Hours: %ld\t Minutes: %ld\t Seconds %ld\n", diff_year, diff_day, diff_hour, diff_min, diff_sec);
            
      sleep(1);
   }
   
   return 0;
}
